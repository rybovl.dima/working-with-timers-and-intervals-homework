// #1

// document.addEventListener('DOMContentLoaded', () => {
//     const changeTextBtn = document.getElementById('change-text-btn');
//     const messageDiv = document.getElementById('message-div');

//     changeTextBtn.addEventListener('click', () => {
//         setTimeout(() => {
//             messageDiv.textContent = 'Операція виповнена вдало!!!!!!!';
//         }, 3000); 
//     });
// });


// #2

// document.addEventListener('DOMContentLoaded', () => {
//     const countdownElement = document.getElementById('countdown');
//     let countdownValue = 10;

//     const countdownInterval = setInterval(() => {
//         countdownValue--;
//         if (countdownValue > 0) {
//             countdownElement.textContent = countdownValue;
//         } else {
//             countdownElement.textContent = "Зворотній відлік завершено";
//             clearInterval(countdownInterval);
//         }
//     }, 1000); 
// });